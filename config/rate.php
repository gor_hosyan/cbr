<?php

return [
    'endpoint' => env('CBR_ENDPOINT', 'http://www.cbr.ru/scripts/XML_daily.asp'),
    'date_format' => 'd/m/Y'
];